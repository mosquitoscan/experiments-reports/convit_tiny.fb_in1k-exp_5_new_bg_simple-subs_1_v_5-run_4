
Model info for convit_tiny.fb_in1k
==================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 192 x 14 x 14  
Conv2d                                    147648     True      
Identity                                                       
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 384      
Linear                                    73728      True      
Linear                                    36864      True      
Dropout                                                        
Linear                                    37056      True      
____________________________________________________________________________
                     64 x 196 x 196 x 4  
Linear                                    16         True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 196 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 196 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 197 x 576      
Linear                                    110592     True      
Dropout                                                        
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 197 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 197 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 197 x 576      
Linear                                    110592     True      
Dropout                                                        
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 197 x 768      
Linear                                    148224     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 197 x 192      
Linear                                    147648     True      
Dropout                                                        
LayerNorm                                 384        True      
Dropout                                                        
Identity                                                       
BatchNorm1d                               384        True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    98304      True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 13             
Linear                                    6656       True      
____________________________________________________________________________

Total params: 5,586,016
Total trainable params: 5,586,016
Total non-trainable params: 0

Optimizer used: <function Adam at 0x79963a903eb0>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback